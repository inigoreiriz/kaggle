# Kaggle competitions #

## Human Resources Analytics

Why are our best and most experienced employees leaving prematurely? We try to predict which valuable employees will leave next. Fields in the dataset include:

1. Employee satisfaction level
2. Last evaluation
3. Number of projects
4. Average monthly hours
5. Time spent at the company
6. Whether they have had a work accident
7. Whether they have had a promotion in the last 5 years
8. Sales
9. Salary
10. Whether the employee has left



